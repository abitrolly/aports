# Contributor: August Klein <amatcoder@gmail.com>
# Maintainer: August Klein <amatcoder@gmail.com>
pkgname=mednafen
pkgver=1.22.2
pkgrel=1
pkgdesc="A command-line multi-system emulator"
url="https://mednafen.github.io/"
arch="all !s390x !mips !mips64"
license="GPL-2.0-only"
makedepends="alsa-lib-dev flac-dev libogg-dev libsndfile-dev libvorbis-dev
	lzo-dev sdl2-dev zlib-dev"
subpackages="$pkgname-lang"
source="https://mednafen.github.io/releases/files/mednafen-$pkgver.tar.xz
	ppc64.patch
	"
builddir="$srcdir"/$pkgname

prepare() {
	default_prepare
	update_config_sub
	update_config_guess
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	make check
}

lang() {
	default_lang
	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$subpkgdir"/usr/share/locale "$subpkgdir"/usr/share/$pkgname
}

package() {
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/lib/charset.alias
}

sha512sums="
e094a9134115344bf857eb7edce67f146fd43b83432db69271d5924ab5ec7dae11cdb7272c0d3c697a654902ce73cb42165f5e1256758f05e41167007e8f3a2d  mednafen-1.22.2.tar.xz
9f4bcab7e1738e6317ce67640534f2bbb3eebc035539e7dccbef2b581d68124ae401420f9254e8daca0b9de76845332b6a8872e402a3fc54f577b31d00b8b858  ppc64.patch
"
