# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-imagick
_extname=imagick
pkgver=3.6.0_rc2
_pkgver=${pkgver/_rc/RC}
pkgrel=0
pkgdesc="PHP 8.1 extension provides a wrapper to the ImageMagick library - PECL"
url="https://pecl.php.net/package/imagick"
arch="all"
license="PHP-3.01"
depends="php81-common imagemagick"
checkdepends="ghostscript-fonts"
makedepends="php81-dev imagemagick-dev"
subpackages="$pkgname-dev"
source="php-pecl-$_extname-$_pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"

build() {
	phpize81
	./configure --prefix=/usr --with-php-config=php-config81
	make
}

check() {
	php81 -dextension=modules/$_extname.so --ri $_extname

	# see https://gitlab.alpinelinux.org/alpine/aports/-/issues/12537
	[ "$CARCH" = x86 ] && rm -f tests/166_Imagick_waveImage_basic.phpt
	make NO_INTERACTION=1 REPORT_EXIT_STATUS=1 test TESTS=--show-diff
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php81/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
caf70da42f2f0eda9113d715d37995137da31730065954a5f3eca81faf7636190563f946772648ac1bec06b76af96fadb092824c6ce39f72684ce4c92e650fd7  php-pecl-imagick-3.6.0RC2.tgz
"
